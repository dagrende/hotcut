#Author dag.rende@gmail.com
#Description generate g-code for hotcut - a Hot Wire Foam Cutter for model airplane wings

import adsk.core, adsk.fusion, adsk.cam, traceback, os

prevPoint = None

def run(context):
    global prevPoint

    ui = None
    prevPoint = None
    try:
        app = adsk.core.Application.get()
        ui  = app.userInterface
        app = adsk.core.Application.get()

        design = app.activeProduct
        if not design:
            ui.messageBox('No active Fusion 360 design', 'No Design')
            return
        
        rootComp = design.rootComponent
        sketches = rootComp.sketches
        if sketches.count > 0:
            sketch = sketches.item(0)
            profiles = sketch.profiles
            if profiles.count > 0:
                profile = profiles.item(0)
                loops = profile.profileLoops
                for i in range(0, loops.count):
                    outerLoop = loops.item(i)
                    if outerLoop.isOuter:
                        break
                profileCurves = outerLoop.profileCurves
                log(rootComp.name)
                with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), rootComp.name + '.nc'), 'w+') as file:
                    i = 0
                    while i < profileCurves.count:
                        curve = profileCurves.item(i)
                        geom = curve.geometry
                        evtor = geom.evaluator
                        (returnValue, startParameter, endParameter) = evtor.getParameterExtents()
                        length = evtor.getLengthAtParameter(startParameter, endParameter)

                        step = (endParameter - startParameter) / 100
                        p = startParameter
                        while p < endParameter:
                            (returnValue, point) = evtor.getPointAtParameter(p)
                            emit(file, point)
                            p += step
                        (returnValue, point) = evtor.getPointAtParameter(endParameter)
                        emit(file, point)
                        i += 1
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

def emit(file, point):
    global prevPoint
    if prevPoint is None or point.distanceTo(prevPoint) > 0.0001:
        file.write(f"g0 x{'{0:.20f}'.format(point.x * 10)} y{'{0:.20f}'.format(point.y * 10)}\n")
        prevPoint = point

def log(msg):
    try:
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'log.txt'), 'a+') as file:
            file.write(msg + "\n")
    except:
        pass



